function setCookie(obj) {    
    document.cookie = obj.id + "=" + obj.checked + "; expires=Wed, 01 Jan 2024 00:00:01 GMT";
}

function restoreCookies() {
    var ca = document.cookie.split('; ');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        c = c.split('=');
        document.getElementById(c[0]).checked = c[1];
    }
}
